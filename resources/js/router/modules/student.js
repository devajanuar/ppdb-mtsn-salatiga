import Layout from '@/layout';

const studentRoutes = {
    path: '/student',
    component: Layout,
    redirect: '/student/list',
    name: 'Student',
    meta: {
        title: 'student',
        icon: 'user'
    },
    children: [
        {
            path: 'list',
            component: () => import('@/views/student/student'),
            name: 'ListStudent',
            meta: {
                title: 'Siswa',
                icon: 'user'
            }
        },
    ]
};
export default studentRoutes;