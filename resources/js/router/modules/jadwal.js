import Layout from '@/layout';

const jadwalRoutes = {
    path: '/jadwal',
    component: Layout,
    redirect: '/jadwal/list',
    name: 'Jadwal',
    meta: {
        title: 'Jadwal',
        icon: 'link',
    },
    children: [
        {
            path: 'list',
            component: () => import('@/views/jadwal/jadwal'),
            name: 'ListJadwal',
            meta: {
                title: 'List Jadwal',
            }
        },
    ],
};
export default jadwalRoutes;