import request from '@/utils/request';

export function getStudent(query){
    return request({
        url: 'student/list',
        method: 'get',
        params: query
    })
}

export function acceptStudent(data){
    return request({
        url: 'student/accept',
        method: 'post',
        data
    })
}

export function registerStudent(data){
	return request({
		url: 'register',
		method: 'post',
		data
	})
}

export function upload(data){
	return request({
		url: 'upload',
		method: 'post',
		data
	})
}

export function dash(query){
	return request({
		url: 'dash',
		method: 'get',
		params: query
	})
}