import request from '@/utils/request';

export function postJadwal(data){
    return request({
        url: 'create',
        method: 'post',
        data
    })
}

export function getJadwal(){
    return request({
        url: 'list',
        method: 'get',
    })
}

export function updateJadwal(data){
    return request({
        url: 'update',
        method: 'post',
        data
    })
}

export function deleteJadwal(id){
    return request({
        url: 'delete/'+ id,
        method: 'post'
    })
}