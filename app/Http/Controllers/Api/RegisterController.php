<?php

namespace App\Http\Controllers\Api;

use App\Laravue\JsonResponse;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Env;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Object_;
use PHPMailer\PHPMailer\PHPMailer;
use Symfony\Component\VarDumper\Cloner\Data;

class RegisterController extends BaseController
{
	public function cekNisn(Request $request)
	{
		$data = DB::table('students')->where('nisn', $request->nisn)->first();

		if ($data) {
			return response()->json(true, 200);
		} else {
			return response()->json(false, 200);
		}
	}

	public function emailBlast(Request $request)
	{
	}

	public function upload(Request $request)
	{
		$request->validate([
			'photos' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);

		$imageName = time() . '.' . $request->photos->extension();

		$request->image->move(public_path('uploads'), $imageName);
		return response()->json($imageName);
	}

	public function store(Request $request)
	{
		$this->validate($request, [
			'email' => 'required',
			'name' => 'required',
			'nisn' => 'required',
			'gender' => 'required',
			'tempat_lahir' => 'required',
			'tanggal_lahir' => 'required',
			'agama' => 'required',
			'contact' => 'required',
			'address' => 'required',
			'province' => 'required',
			'kota' => 'required',
			'kecamatan' => 'required',
			'kelurahan' => 'required',
			'peminatan' => 'required',
			'asal_sekolah' => 'required',
			'bi_4_sem1' => 'required',
			'bi_5_sem1' => 'required',
			'bi_6_sem1' => 'required',
			'mat_4_sem1' => 'required',
			'mat_5_sem1' => 'required',
			'mat_6_sem1' => 'required',
			'ipa_4_sem1' => 'required',
			'ipa_5_sem1' => 'required',
			'ipa_6_sem1' => 'required',
			'bi_4_sem1' => 'required',
			'bi_5_sem1' => 'required',
			'bi_6_sem1' => 'required',
			'mat_4_sem2' => 'required',
			'mat_5_sem2' => 'required',
			'ipa_4_sem2' => 'required',
			'ipa_5_sem2' => 'required',
		]);

		$daftar = DB::table('students')->latest('id')->first();
		$ratus = '000';
		$puluh = '00';
		$satuan = '0';

		$response = new Object_();

		if ($daftar == null) {
			$nomor_pendaftaran = 'MTsN-21-' . $ratus . '1';
			$pass = '21' . $ratus . '1';
		} else {
			if ($daftar->id < 10) {
				$nomor_pendaftaran = 'MTsN-21-' . $ratus . ($daftar->id + 1);
				$pass = '21' . $ratus . ($daftar->id + 1);
			} else if ($daftar->id < 100) {
				$nomor_pendaftaran = 'MTsN-21-' . $puluh . ($daftar->id + 1);
				$pass = '21' . $puluh . ($daftar->id + 1);
			} else {
				$nomor_pendaftaran = 'MTsN-21-' . $satuan . ($daftar->id + 1);
				$pass = '21' . $satuan . ($daftar->id + 1);
			}
		}

		$inc = 1;
		$jadwal1 = DB::table('students')->where('jadwal_tes', 1)->count();
		$jadwal2 = DB::table('students')->where('jadwal_tes', 2)->count();
		$jadwal3 = DB::table('students')->where('jadwal_tes', 3)->count();
		$jadwal4 = DB::table('students')->where('jadwal_tes', 4)->count();
		$jadwal5 = DB::table('students')->where('jadwal_tes', 5)->count();
		$jadwal6 = DB::table('students')->where('jadwal_tes', 6)->count();
		$jadwal7 = DB::table('students')->where('jadwal_tes', 7)->count();
		$jadwal8 = DB::table('students')->where('jadwal_tes', 8)->count();
		$jadwal9 = DB::table('students')->where('jadwal_tes', 9)->count();
		$jadwal10 = DB::table('students')->where('jadwal_tes', 10)->count();
		$jadwal11 = DB::table('students')->where('jadwal_tes', 11)->count();
		$jadwal12 = DB::table('students')->where('jadwal_tes', 12)->count();
		$jadwal13 = DB::table('students')->where('jadwal_tes', 13)->count();
		$jadwal14 = DB::table('students')->where('jadwal_tes', 14)->count();
		$jadwal15 = DB::table('students')->where('jadwal_tes', 15)->count();
		$jadwal16 = DB::table('students')->where('jadwal_tes', 16)->count();
		$jadwal17 = DB::table('students')->where('jadwal_tes', 17)->count();
		$jadwal18 = DB::table('students')->where('jadwal_tes', 18)->count();
		$jadwal19 = DB::table('students')->where('jadwal_tes', 19)->count();
		$jadwal20 = DB::table('students')->where('jadwal_tes', 20)->count();
		$jadwal21 = DB::table('students')->where('jadwal_tes', 21)->count();
		$jadwal22 = DB::table('students')->where('jadwal_tes', 22)->count();
		$jadwal23 = DB::table('students')->where('jadwal_tes', 23)->count();
		$jadwal24 = DB::table('students')->where('jadwal_tes', 24)->count();
		$val = 0;

		if ($jadwal1 <= 30) {
			$val = 1;
		}
		if ($jadwal1 == 30 && $jadwal2 <= 30) {
			$val = 2;
		}
		if ($jadwal2 == 30 && $jadwal3 <= 30) {
			$val = 3;
		}
		if ($jadwal3 == 30 && $jadwal4 <= 30) {
			$val = 4;
		}
		if ($jadwal4 == 30 && $jadwal5 <= 30) {
			$val = 5;
		}
		if ($jadwal5 == 30 && $jadwal6 <= 30) {
			$val = 6;
		}
		if ($jadwal6 == 30 && $jadwal7 <= 30) {
			$val = 7;
		}
		if ($jadwal7 == 30 && $jadwal8 <= 30) {
			$val = 8;
		}
		if ($jadwal8 == 30 && $jadwal9 <= 30) {
			$val = 9;
		}
		if ($jadwal9 == 30 && $jadwal10 <= 30) {
			$val = 10;
		}
		if ($jadwal10 == 30 && $jadwal11 <= 30) {
			$val = 11;
		}
		if ($jadwal11 == 30 && $jadwal12 <= 30) {
			$val = 12;
		}
		if ($jadwal12 == 30 && $jadwal13 <= 30) {
			$val = 13;
		}
		if ($jadwal13 == 30 && $jadwal14 <= 30) {
			$val = 14;
		}
		if ($jadwal14 == 30 && $jadwal15 <= 30) {
			$val = 15;
		}
		if ($jadwal15 == 30 && $jadwal16 <= 30) {
			$val = 16;
		}
		if ($jadwal16 == 30 && $jadwal17 <= 30) {
			$val = 17;
		}
		if ($jadwal17 == 30 && $jadwal18 <= 30) {
			$val = 18;
		}
		if ($jadwal18 == 30 && $jadwal19 <= 30) {
			$val = 19;
		}
		if ($jadwal19 == 30 && $jadwal20 <= 30) {
			$val = 20;
		}
		if ($jadwal20 == 30 && $jadwal21 <= 30) {
			$val = 21;
		}
		if ($jadwal21 == 30 && $jadwal22 <= 30) {
			$val = 22;
		}
		if ($jadwal22 == 30 && $jadwal23 <= 30) {
			$val = 23;
		}
		if ($jadwal23 == 30 && $jadwal24 <= 30) {
			$val = 24;
		}

		//Append Email Content
		$request->nomor_pendaftaran = $nomor_pendaftaran;
		$form = PDF::loadView('form', ['data' => $request, 'nomor_pendaftaran' => $nomor_pendaftaran, 'tanggal_lahir' => Carbon::parse($request->tanggal_lahir)->format('d-M-Y')])->save('pdf' . '/form-' . $nomor_pendaftaran . '.pdf');

		$jad = DB::table('schedules')->join('detail_schedules', 'schedules.id', 'detail_schedules.schedule')->where('schedules.id', $val)->first();
		$kartu = PDF::loadView('kartu', ['data' => $request, 'nomor_pendaftaran' => $nomor_pendaftaran, 'jadwal' => $jad, 'pelaksanaan' => Carbon::parse($jad->date)->format('d-M-Y'), 'pass' => $pass, 'tanggal_lahir' => Carbon::parse($request->tanggal_lahir)->format('d-M-Y')])->save('pdf' . '/kartu-' . $nomor_pendaftaran . '.pdf');

		if ($form && $kartu) {
			$cek = DB::table('students')->where('nisn', $request->nisn)->first();

			if ($cek) {
				$message = "Nomor Induk Siswa sudah terdaftar";
				$status = false;

				$response->message = $message;
				$response->status = $status;
				return response()->json(new JsonResponse([], $message));
			}

			$email = strtolower($request->email);
			$mail = $this->registration_mail($request, $nomor_pendaftaran);
			return response()->json($mail, 400);
			
			$student = DB::table('students')->insertGetId([
				'photo' => $request->photos,
				'nomor_pendaftaran' => $nomor_pendaftaran,
				'name' => $request->name,
				'nisn' => $request->nisn,
				'nick_name' => $request->nick_name,
				'gender' => $request->gender,
				'tempat_lahir' => $request->tempat_lahir,
				'tanggal_lahir' => Carbon::parse($request->tanggal_lahir)->format('Y-m-d'),
				'email' => str_replace(' ', '', $email),
				'contact' => $request->contact,
				'nomor_nik' => $request->no_nik,
				'agama' => $request->agama,
				'jadwal_tes' => $val,
				'peminatan' => $request->peminatan,
				'asal_sekolah' => $request->asal_sekolah
			]);

			try {
				DB::table('student_address')->insert([
					'student' => $student,
					'address' => 'Provinsi ' . $request->province . ' Kabupaten/Kota ' . $request->kota . ' Kecamatan ' . $request->kecamatan . ' Kelurahan ' . $request->kelurahan . ' RT' . $request->rt . '/RW' . $request->rw . $request->address,
					'phone_number' => $request->phone_number
				]);

				if ($request->nama_ayah != '') {
					DB::table('student_parents')->insert([
						'student' => $student,
						'name' => $request->nama_ayah,
						'tempat_lahir' => $request->tempat_lahir_ayah,
						'tanggal_lahir' => Carbon::parse($request->tanggal_lahir_ayah)->format('Y-m-d'),
						'job' => $request->job_ayah,
						'contact' => $request->contact_ayah,
						'religion' => $request->agama_ayah,
						'salary' => $request->salary_ayah,
					]);
				}
				if ($request->nama_ibu != '') {
					DB::table('student_parents')->insert([
						'student' => $student,
						'name' => $request->nama_ibu,
						'tempat_lahir' => $request->tempat_lahir_ibu,
						'tanggal_lahir' => Carbon::parse($request->tanggal_lahir_ibu)->format('Y-m-d'),
						'job' => $request->job_ibu,
						'contact' => $request->contact_ibu,
						'religion' => $request->agama_ibu,
						'salary' => $request->salary_ibu,
					]);
				}

				if ($request->school == 'MI') {
					DB::table('student_rapor_iv')->insert([
						'student' => $student,
						'school_type' => $request->school,
						'bi_sem1' => $request->bi_4_sem1,
						'bi_sem2' => $request->bi_4_sem2,
						'mat_sem1' => $request->mat_4_sem1,
						'mat_sem2' => $request->mat_4_sem2,
						'ipa_sem1' => $request->ipa_4_sem1,
						'ipa_sem2' => $request->ipa_4_sem2,
						'hadist_sem1' => $request->hadis_4_sem1,
						'hadist_sem2' => $request->hadis_4_sem2,
						'akidah_sem1' => $request->akidah_4_sem1,
						'akidah_sem2' => $request->akidah_4_sem2,
						'fiqih_sem1' => $request->fiqih_4_sem1,
						'fiqih_sem2' => $request->fiqih_4_sem2,
						'ski_sem1' => $request->ski_4_sem1,
						'ski_sem2' => $request->ski_4_sem2,
					]);

					DB::table('student_rapor_v')->insert([
						'student' => $student,
						'school_type' => $request->school,
						'bi_sem1' => $request->bi_5_sem1,
						'bi_sem2' => $request->bi_5_sem2,
						'mat_sem1' => $request->mat_5_sem1,
						'mat_sem2' => $request->mat_5_sem2,
						'ipa_sem1' => $request->ipa_5_sem1,
						'ipa_sem2' => $request->ipa_5_sem2,
						'hadist_sem1' => $request->hadis_5_sem1,
						'hadist_sem2' => $request->hadis_5_sem2,
						'akidah_sem1' => $request->akidah_5_sem1,
						'akidah_sem2' => $request->akidah_5_sem2,
						'fiqih_sem1' => $request->fiqih_5_sem1,
						'fiqih_sem2' => $request->fiqih_5_sem2,
						'ski_sem1' => $request->ski_5_sem1,
						'ski_sem2' => $request->ski_5_sem2,
					]);

					DB::table('student_rapor_vi')->insert([
						'student' => $student,
						'school_type' => $request->school,
						'bi_sem1' => $request->bi_6_sem1,
						'mat_sem1' => $request->mat_6_sem1,
						'ipa_sem1' => $request->ipa_6_sem1,
						'hadist_sem1' => $request->hadis_6_sem1,
						'akidah_sem1' => $request->akidah_6_sem1,
						'fiqih_sem1' => $request->fiqih_6_sem1,
						'ski_sem1' => $request->ski_6_sem1
					]);
				} else {
					DB::table('student_rapor_iv')->insert([
						'student' => $student,
						'school_type' => $request->school,
						'bi_sem1' => $request->bi_4_sem1,
						'bi_sem2' => $request->bi_4_sem2,
						'mat_sem1' => $request->mat_4_sem1,
						'mat_sem2' => $request->mat_4_sem2,
						'ipa_sem1' => $request->ipa_4_sem1,
						'ipa_sem2' => $request->ipa_4_sem2,
						'agama_sem1' => $request->agama_4_sem1,
						'agama_sem2' => $request->agama_4_sem2
					]);

					DB::table('student_rapor_v')->insert([
						'student' => $student,
						'school_type' => $request->school,
						'bi_sem1' => $request->bi_5_sem1,
						'bi_sem2' => $request->bi_5_sem2,
						'mat_sem1' => $request->mat_5_sem1,
						'mat_sem2' => $request->mat_5_sem2,
						'ipa_sem1' => $request->ipa_5_sem1,
						'ipa_sem2' => $request->ipa_5_sem2,
						'agama_sem1' => $request->agama_5_sem1,
						'agama_sem2' => $request->agama_5_sem2
					]);

					DB::table('student_rapor_vi')->insert([
						'student' => $student,
						'school_type' => $request->school,
						'bi_sem1' => $request->bi_6_sem1,
						'mat_sem1' => $request->mat_6_sem1,
						'ipa_sem1' => $request->ipa_6_sem1,
						'agama_sem1' => $request->agama_6_sem1,
					]);
				}

				if ($request->tingkat1 != '' && $request->prestasi1 != '') {
					DB::table('prestasis')->insert([
						'student' => $student,
						'tingkat' => $request->tingkat1,
						'keterangan' => $request->prestasi1
					]);
				}

				if ($request->tingkat2 != '' && $request->prestasi2 != '') {
					DB::table('prestasis')->insert([
						'student' => $student,
						'tingkat' => $request->tingkat2,
						'keterangan' => $request->prestasi2
					]);
				}

				if ($request->tingkat3 != '' && $request->prestasi3 != '') {
					DB::table('prestasis')->insert([
						'student' => $student,
						'tingkat' => $request->tingkat3,
						'keterangan' => $request->prestasi3
					]);
				}
			} catch (\Throwable $th) {
				return response()->json(new JsonResponse([], $th->getMessage()));
			}

			$status = true;
			$response->status = $status;
			$response->name = $request->name;
			$response->email = $request->email;

			if ($mail) {
				return response()->json(new JsonResponse(['items' => $response]));
			} else {
				return response()->json(new JsonResponse([], $mail));
			}
		} else {
			return response()->json(new JsonResponse([], 'Tidak dapat mengirim data, pastikan koneksi anda lancar'));
		}
	}

	public function list(Request $request)
	{
		$nisn = $request->input('nisn');

		if($nisn != null || $nisn != ''){
			$data = DB::table('students')->join('student_address', 'students.id', 'student_address.student')
			->join('schedules', 'students.jadwal_tes', 'schedules.id')
			->join('detail_schedules', 'schedules.id', 'detail_schedules.schedule')
			->select([
				'students.id',
				'students.name',
				'students.nomor_pendaftaran',
				'students.nisn',
				'schedules.name as jadwal'
			])
			->where('students.nisn', $nisn)
			->get();
		}else{
			$data = DB::table('students')->join('student_address', 'students.id', 'student_address.student')
			->join('schedules', 'students.jadwal_tes', 'schedules.id')
			->join('detail_schedules', 'schedules.id', 'detail_schedules.schedule')
			->select([
				'students.id',
				'students.name',
				'students.nomor_pendaftaran',
				'students.nisn',
				'schedules.name as jadwal'
			])
			->get();
		}

		$total = DB::table('students')->count();

		return response()->json(new JsonResponse(['items' => $data,
		'total' => $total]));
	}

	public function dash(Request $request)
	{
		$data = DB::table('students')->count();
		$date = Carbon::now()->format('Y');

		return response()->json(new JsonResponse([
			'student' => $data,
			'date' => $date
			]));
	}

	public function sendMail($data)
	{
		$mail = new PHPMailer(true);
		try {
			$email = 'noreply@ppdb.mtsnegerisalatiga.sch.id';
			$name = 'PPDB MtsN Salatiga';
			$mail->isSMTP();
			$mail->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => false
				)
			);
			$mail->CharSet = 'utf-8';
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = null;
			$mail->Host = 'mail.ppdb.mtsnegerisalatiga.sch.id'; //gmail has host > smtp.gmail.com
			$mail->Port = 587; //gmail has port > 587 . without double quotes
			$mail->Username = 'noreply@ppdb.mtsnegerisalatiga.sch.id'; //your username. actually your email
			$mail->Password = 'pf]r(x!{MT3h'; // your password. your mail password
			$mail->setFrom($email, $name);
			$mail->Subject = $data['subject'] ?? "";
			$mail->msgHTML($data['message'] ?? "");
			$mail->addAddress($data['to'] ?? "emailnotvalid@gmail.com");
			$mail->addAttachment('pdf/' . 'form-' . $data['nomor'] . '.pdf', 'form-' . $data['nomor'] . '.pdf', 'base64', 'application/pdf');
			$mail->addAttachment('pdf/' . 'kartu-' . $data['nomor'] . '.pdf', 'kartu-' . $data['nomor'] . '.pdf', 'base64', 'application/pdf');
			$mail->send();

			if ($mail) {
				return true;
			} else {
				return false;
			}
		} catch (\Exception $e) {
			return false;
		}
	}

	public function acceptStudentMail(object $data)
	{
		$mail = new RegisterController;
		$data = (object) $data;
		$fullname = $data->fullname ?? null;
		$sekolah = $data->school_name ?? null;

		$logo = env('APP_DEFAULT_LOGO');

		$pesan = '<div style="display:block;align:center"><img src="' . $logo . '" style="display:block; margin:20px auto 20px auto; height:100px; width:auto;"></div>
                <div style="text-align:center;display:block;font-size:14px">
                <center><p>Halo, <strong>' . $fullname . '</strong></p><p></p></center>
                <p>Selamat anda di terima sebagai Siswa</p>
                <p>di sekolah ' . $sekolah . '</p>
                </div>
                <div style="text-align:center;min-height:50px; display:block; background:#FFFAFA; padding:10px; font-family:Arial; font-size:12px; color:#000;  -webkit-border-radius: 10px; -moz-border-radius: 10px; border-radius: 10px;">
                <p><img src="https://sistesi.s3-ap-southeast-1.amazonaws.com/mistar-assets/PNG/email_mistar.png" style="display:block; margin:20px auto 20px auto; height:100px; width:auto;">
                <p>Silahkan datang ke sekolah kami</p>
                <p>untuk melanjutkan tahapan selanjutnya</p>
                <p><hr style="width:20%;text-align:center;"></p>
                <p>Untuk login ke web sekolah kami</p>
                <p>klik tombol di bawah ini</p>
                <center><p><a href="' . $institution->base_url_fe . 'login" style="background:#0000FF; color:#FFF; font-size:14px; padding:8px 10px 8px 10px; margin:20px 0 20px 0; display: block; width:150px; text-align:center; text-decoration:none; -webkit-border-radius: 10px; -moz-border-radius: 10px; border-radius: 10px;" target="_blank"><strong>LOGIN</strong></a></p></center>
                </div>
                <div style="text-align:center;min-height:50px; display:block; background:#FFFAFA; padding:10px; font-size:14px; color:#000;  -webkit-border-radius: 10px; -moz-border-radius: 10px; border-radius: 10px; margin-top: 20px">
                <p>Dapatkan Aplikasi<strong> MISTAR</strong></p>
                <p>Jika anda sudah mendownload, anda bisa login dengan</p>
                <p>akun yang sudah anda buat</p>
                <a href="https://play.google.com/store/apps/details?id=id.co.mistar"><img src="https://sistesi.s3-ap-southeast-1.amazonaws.com/mistar-assets/PNG/GP.png" style="height:50px;width:150px"></a>
                <a href="https://apps.apple.com/us/app/mistar/id1534293138"><img src="https://sistesi.s3-ap-southeast-1.amazonaws.com/mistar-assets/PNG/AP.png" style="height:50px;width:150px">
                </div>
                <div style="text-align:center; margin-top: 20px">
                <a href="https://twitter.com/exiglosioffici4"><img src="https://sistesi.s3-ap-southeast-1.amazonaws.com/mistar-assets/PNG/tw.png" style="height:30px;width:30px"></a>
                <a href="https://www.instagram.com/exiglosiofficial/"><img src="https://sistesi.s3-ap-southeast-1.amazonaws.com/mistar-assets/PNG/ig.png" style="height:30px;width:30px"></a>
                <a href="https://www.youtube.com/channel/UCVls62jm2a2zERZYB_Z5jKA"><img src="https://sistesi.s3-ap-southeast-1.amazonaws.com/mistar-assets/PNG/YT.png" style="height:30px;width:30px"></a>
                <a href="https://www.facebook.com/exiglosiofficial/"><img src="https://sistesi.s3-ap-southeast-1.amazonaws.com/mistar-assets/PNG/FB.png" style="height:30px;width:30px"></a>
                </div>
                <div style="text-align:center;font-size:11px;">
                <p>Copyright 2020</p>
                <a href="https://www.exiglosi.com"><img src="https://sistesi.s3-ap-southeast-1.amazonaws.com/mistar-assets/PNG/exi.png" style="height:50px;width:auto"></a>
                </div>';

		$data_send = [
			'subject' => 'Registration Announcement',
			'message' => $pesan ?? "",
			'to' => $data->to ?? ""
		];

		return $mail->sendMail($data_send);
	}

	public function registration_mail($request, $nomor_pendaftaran)
	{
		$pesan = '
                <div style="display:block;"><img src="https://ppdb.mtsnegerisalatiga.sch.id/images/mtslogo.png" style="display:block; margin:20px auto 20px auto; height:100px; width:auto;"></div>
                <div style="min-height:50px; display:block; background:#f1f1f1; padding:10px; font-family:Arial; font-size:12px; color:#000;  -webkit-border-radius: 10px; -moz-border-radius: 10px; border-radius: 10px;">
                <p>Halo, <strong>' . $request->name . '</strong></p><p></p>
                <p>Terima kasih anda telah terdaftar sebagai calon siswa baru di MTs Negeri Salatiga</p>
                <p>Nomor Pendaftaran: </p>
                <p><h3>' . $nomor_pendaftaran . '</h3></p>
                <p></p>
                <p>Pastikan anda mencetak serta membawa kartu tes yang bisa di download pada email ini</p>
				<p><h2 style="color:red">Berkas pendaftaran mohon dibawa pada saat seleksi</h2></p>
                <p>Salam,</p>
                <p>Administrator</p>
                </div>
                <div style="display:block; margin:20px 0 20px 0; font-family:Arial; font-size:11px;">
                Website : <a href="https://www.mtsnegerisalatiga.sch.id">Mts Negeri Salatiga</a>
                </p>
                </div>
                ';

		$data_send = [
			'subject' => 'Pendaftaran Siswa Baru',
			'message' => $pesan,
			'to' => $request->email,
			'nomor' => $nomor_pendaftaran
		];
		$res = $this->sendMail($data_send);

		return $res;
	}
}
