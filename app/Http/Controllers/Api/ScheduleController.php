<?php

namespace App\Http\Controllers\Api;

use App\Laravue\JsonResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ScheduleController extends BaseController
{
    public function store(Request $request)
    {
        try {
            $schedule = DB::table('schedules')->insertGetId([
                'name' => $request->name
            ]);

            $detail = DB::table('detail_schedules')->insert([
                'schedule' => $schedule,
                'keterangan' => $request->keterangan,
				'date' => Carbon::parse($request->date)->format('Y-m-d'),
                'start_time' => $request->start_time,
                'end_time' => $request->end_time
            ]);

            return response()->json(new JsonResponse([]));
        } catch (\Throwable $th) {
            return response()->json(new JsonResponse([],$th->getMessage()));
        }
    }

    public function index(Request $request)
    {
        try {
            $data = DB::table('schedules')->join('detail_schedules', 'schedules.id', 'detail_schedules.schedule')->paginate(10);

            return response()->json(new JsonResponse(['items' => $data]));
        } catch (\Throwable $th) {
            return response()->json(new JsonResponse([], $th->getMessage()));
        }
    }

	public function update(Request $request)
	{
		try {
			DB::table('schedules')->where('id', $request->id)->update([
				'name' => $request->name
			]);

			DB::table('detail_schedules')->where('schedule', $request->id)->update([
                'keterangan' => $request->keterangan,
				'date' => Carbon::parse($request->date)->format('Y-m-d'),
                'start_time' => $request->start_time,
                'end_time' => $request->end_time
            ]);

			return response()->json(new JsonResponse([]));
		} catch (\Throwable $th) {
			return response()->json(new JsonResponse([], $th->getMessage()));
		}
	}

    public function destroy(Request $request)
    {
        try {
            DB::table('detail_schedules')->where('schedule', $request->id)->delete();
            DB::table('schedules')->where('id', $request->id)->delete();

            return response()->json(new JsonResponse([]));
        } catch (\Throwable $th) {
            return response()->json(new JsonResponse([], $th->getMessage()));
        }
    }
}
